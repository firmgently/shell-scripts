#!/bin/bash
logger -p info "$0 $*"

# only hit hetzner server one script at a time
(
flock -n 9 || fail "another script accessing hetzner"

# rsync options:
# -r          [recurse] into directories
# -u          skip files that are newer on the receiver [update]
# -t          preserve modification [times]
# -v          [verbose]
# --inplace   update destination files in-place
#             This option changes how rsync transfers a file when the file's data needs to be updated: instead of the default method of creating a new copy of the file and moving it into place when it is complete, rsync instead writes the updated data directly to the destination file.
#             This has several effects:
#               (1) in-use binaries cannot be updated (either the OS will prevent this from happening, or binaries that attempt to swap-in their data will misbehave or crash)
#               (2) the file's data will be in an inconsistent state during the transfer
#               (3) a file's data may be left in an inconsistent state after the transfer if the transfer is interrupted or if an update fails
#               (4) a file that does not have write permissions can not be updated, and
#               (5) the efficiency of rsync's delta-transfer algorithm may be reduced if some data in the destination file is overwritten before it can be copied to a position later in the file (one exception to this is if you combine this option with --backup, since rsync is smart enough to use the backup file as the basis file for the transfer).
#             WARNING: you should not use this option to update files that are being accessed by others, so be careful when choosing to use this for a copy.
#             This option is useful for transfer of large files with block-based changes or appended data, and also on systems that are disk bound, not network bound.
#             The option implies --partial (since an interrupted transfer does not delete the file), but conflicts with --partial-dir and --delay-updates.
#             Prior to rsync 2.6.4 --inplace was also incompatible with --compare-dest and --link-dest.


# Pull from remote to local
rsync -rutv /mnt/000-HETZNER/ /secondary/hetzner-local/

# Push from local to remote
rsync -rutv --inplace /secondary/hetzner-local/ /mnt/000-HETZNER/

) 9>/var/lock/hetzner
