# Void / i3 / System Scripts

A collection of shell scripts, mainly Bash and a couple of Python, used to help control and configure my systems.

I'm usually on Void Linux with i3 and don't have a full Desktop Environment, so some of the scripts do things which would come ready out-of-the-box on other distributions.

These are mostly written to be used on a Fujitsu Q737 X86 tablet or a ThinkPad X230 laptop, although they should work on other systems with a couple of tweaks (usually noted in the comments)

A lot of them were cobbled together from StackOverflow and Reddit posts, I've tried to note where that's the case but may have missed some as these are things I've been using personally for years.

I'm not laying claim to any of it and if you see something that you feel should be attributed to someone let me know. I'm only sharing these in case they are useful to somebody.
